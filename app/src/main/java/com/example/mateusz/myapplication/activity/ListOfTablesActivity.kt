package com.example.mateusz.myapplication.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.mateusz.myapplication.*
import com.example.mateusz.myapplication.adapters.ExpandableListViewAdapter
import com.example.mateusz.myapplication.models.database.DbInfo
import com.example.mateusz.myapplication.enums.QueryType
import com.example.mateusz.myapplication.interfaces.DbInfoResponse
import com.example.mateusz.myapplication.utils.ConnectionHandler
import com.example.mateusz.myapplication.utils.QueryExecutor
import kotlinx.android.synthetic.main.activity_list_of_tables.*

class ListOfTablesActivity : AppCompatActivity(), DbInfoResponse {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_tables)
        val dbInfo = (application as ConnectionHandler).dbInfo
        if(dbInfo == null) {
            val queryExecutor = QueryExecutor(applicationContext, (application as ConnectionHandler).credentials, QueryType.GET_DB_INFO)
            queryExecutor.dbInfoResponse = this@ListOfTablesActivity
            queryExecutor.execute()
        }
        else{
            elvListOfTables.setAdapter(ExpandableListViewAdapter(dbInfo.getTablesNames(), dbInfo.getColumnNameAndTypePair(), this))
        }
    }

    override fun returnDbInfo(dbInfo: DbInfo) {
        elvListOfTables.setAdapter(ExpandableListViewAdapter(dbInfo.getTablesNames(), dbInfo.getColumnNameAndTypePair(), this))
    }
}
