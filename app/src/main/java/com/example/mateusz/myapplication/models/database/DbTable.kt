package com.example.mateusz.myapplication.models.database

import java.util.ArrayList

/**
 * Describe meta date about table
 */
data class DbTable(val name: String, var columns: ArrayList<DbColumn>) {}
