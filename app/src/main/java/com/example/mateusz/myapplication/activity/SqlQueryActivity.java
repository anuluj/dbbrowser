package com.example.mateusz.myapplication.activity;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mateusz.myapplication.utils.ConnectionClass;
import com.example.mateusz.myapplication.utils.ConnectionHandler;
import com.example.mateusz.myapplication.utils.MyWatcher;
import com.example.mateusz.myapplication.utils.QueryExecutor;
import com.example.mateusz.myapplication.interfaces.QueryResponse;
import com.example.mateusz.myapplication.R;
import com.example.mateusz.myapplication.utils.SpaceTokenizer;
import com.example.mateusz.myapplication.utils.Stopwatch;
import com.example.mateusz.myapplication.models.database.DbInfo;
import com.example.mateusz.myapplication.enums.QueryType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlQueryActivity extends AppCompatActivity implements QueryResponse {
    private Pattern PATTERN_KEYWORDS_PINK, PATTERN_KEYWORDS_BLUE, PATTERN_KEYWORDS_GREY, PATTERN_COMMENTS, PATTERM_STRINGS;
    private String[] keyWords;

    private Stopwatch sw;
    private List<String> autoCompleteWords;
    ScrollView scrollView;
    HorizontalScrollView horizontalScrollView;
    TableLayout tableLayout;
    MultiAutoCompleteTextView mt;
    Context context;
    int numberOfCheckedWords = -1;
    Guideline gl;
    ConstraintLayout.LayoutParams lp;
    DisplayMetrics metrics = new DisplayMetrics();
    Button resizeBt, btExecuteQuery;
    QueryExecutor queryExecutor;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql_query);
        context = getApplicationContext();
        keyWords = context.getResources().getStringArray(R.array.all_key_words);
        autoCompleteWords = new ArrayList<>(Arrays.asList(keyWords));

        DbInfo dbInfo = ((ConnectionHandler)getApplication()).getDbInfo();
        if(dbInfo != null) {
            List<String> columnNames = dbInfo.getFlatColumnNames();
            List<String> tableNames = dbInfo.getTablesNames();
            autoCompleteWords.addAll(columnNames);
            autoCompleteWords.addAll(tableNames);
            android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(dbInfo.getName());
        }

        PATTERN_KEYWORDS_PINK = Pattern.compile(getString(R.string.key_words_pink));
        PATTERN_KEYWORDS_BLUE = Pattern.compile(getString(R.string.key_words_blue));
        PATTERN_KEYWORDS_GREY = Pattern.compile(getString(R.string.key_words_grey));
        PATTERM_STRINGS = Pattern.compile(getString(R.string.regex_strings));

//        toolbar = findViewById(R.id.toolbar);
//        toolbar.setTitle("Query");
//        toolbar.inflateMenu(R.menu.menu_toolbat_sql_query);
//        setActionBar(toolbar);
        tableLayout =  findViewById(R.id.table);
        horizontalScrollView =  findViewById(R.id.horizontalScrollView);
        scrollView =  findViewById(R.id.scrollView);

        progressBar =  findViewById(R.id.progressBarSqlQueryActivity);
        progressBar.setVisibility(View.GONE);
        resizeBt =  findViewById(R.id.button);
        btExecuteQuery = findViewById(R.id.btExecute);
        gl = new Guideline(this);
        gl = findViewById(R.id.guideline2);
        mt = findViewById(R.id.multiAutoCompleteTextView1);
        mt.setTokenizer(new SpaceTokenizer());


        ArrayAdapter<String> adp = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, autoCompleteWords);

        mt.setThreshold(1);
        mt.setAdapter(adp);

        mt.setText("SELECT * FROM actor");
        //--------TEST----------

        mt.addTextChangedListener(new MyWatcher() {
            @Override
            protected void onTextChanged(String before, String old, String aNew, String after) {
                String allWords[] = mt.getText().toString().split("\\s"); //slowa do osobnej tablicy
                Spannable spannableString = new SpannableString(mt.getText()); //tekst do spannable
                String fullText = mt.getText().toString().toUpperCase(); //tekst do stringa
                ArrayList<String> arrayOfCheckedWords = findKeyWords(keyWords, allWords); // znalesc slowa kluczowe w tekscie
                Log.d("Check", "onTextChanged: numberOfCheckedWords" + numberOfCheckedWords);
                Log.d("Check", "onTextChanged: arrayOfCheckedWords.size()" + arrayOfCheckedWords.size());
                if ((arrayOfCheckedWords.size() >= 0 && numberOfCheckedWords != arrayOfCheckedWords.size()) || fullText.contains("\'")) {
                    numberOfCheckedWords = arrayOfCheckedWords.size();
                    clearSpans(spannableString);
                    for (Matcher m = PATTERN_KEYWORDS_BLUE.matcher(fullText); m.find(); ) {
                        spannableString.setSpan(new ForegroundColorSpan(Color.BLUE), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        Log.d("MatcherBLUE: ", "onTextChanged: Start: " + m.start() + "  END: " + m.end());
                    }
                    for (Matcher m = PATTERN_KEYWORDS_GREY.matcher(fullText).useAnchoringBounds(true); m.find(); ) {
                        spannableString.setSpan(new ForegroundColorSpan(Color.GRAY), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        Log.d("MatcherGRAY: ", "onTextChanged: Start: " + m.start() + "  END: " + m.end());
                    }
                    for (Matcher m = PATTERN_KEYWORDS_PINK.matcher(fullText); m.find(); ) {
                        spannableString.setSpan(new ForegroundColorSpan(Color.MAGENTA), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        Log.d("MatcherPINK: ", "onTextChanged: Start: " + m.start() + "  END: " + m.end());
                    }
                    for(Matcher m = PATTERM_STRINGS.matcher(fullText); m.find();){
                        spannableString.setSpan(new ForegroundColorSpan(Color.RED), m.start(), m.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        Log.d("MatcherRED: ", "onTextChanged: Start: " + m.start() + "  END: " + m.end());
                    }

                    startUpdates();
                    int selection = mt.getSelectionStart();
                    mt.setText(spannableString);
                    mt.setSelection(selection);
                    endUpdates();
                }
            }
        });

        //TODO: poprawic zmiane rozmiaru okna // przetestowac na innych urzadzeniach
        resizeBt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int displayHeight = metrics.heightPixels;
                lp = (ConstraintLayout.LayoutParams) gl.getLayoutParams();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float y = event.getRawY();
                        int position = (int) y-110;
                        if (y > 500 && y < displayHeight - 400) {
                            lp.guideBegin = position;
                        }
                        gl.setLayoutParams(lp);
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;

            }
        });
        btExecuteQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //wywolac zapytanie
                sw = new Stopwatch();
                sw.start();
                btExecuteQuery.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                tableLayout.removeAllViewsInLayout();
                String query;
                String selection = mt.getText().toString().substring(mt.getSelectionStart(), mt.getSelectionEnd());

                query = selection.length() > 0 ? selection : mt.getText().toString();

                queryExecutor = new QueryExecutor(query, ((ConnectionHandler) getApplication()).getCredentials(), QueryType.NORMAL_QUERY);
                queryExecutor.delegate = SqlQueryActivity.this;
                queryExecutor.execute();
            }
        });
    }

    ArrayList<String> findKeyWords(String keyWord[], String wordsFromTextBox[]) {
        ArrayList<String> listOfSqlWords = new ArrayList<>();
        Log.d("findKeyWords", "findKeyWords: innitial KeyWordArray: " + keyWord.length);
        Log.d("findKeyWords", "findKeyWords: innitial wordsFromTextBoxArray: " + wordsFromTextBox.length);
        for (int i = 0; i < keyWord.length; i++)
            for (int j = 0; j < wordsFromTextBox.length; j++) {
                if (Objects.equals(keyWord[i].toLowerCase(), wordsFromTextBox[j].toLowerCase())) {
                    listOfSqlWords.add(wordsFromTextBox[j].toUpperCase());
                    Log.d("findKeyWords: ", wordsFromTextBox[j] + "  " + listOfSqlWords.size());
                }
            }
        return listOfSqlWords;
    }

    private static void clearSpans(Spannable e) {
        // remove foreground color spans
        {
            ForegroundColorSpan spans[] = e.getSpans(
                    0,
                    e.length(),
                    ForegroundColorSpan.class);
            for (int n = spans.length; n-- > 0; )
                e.removeSpan(spans[n]);
        }
    }

    @Override
    public void processFinish(ArrayList<String> output, int rows, int columns) {
        //wlaczyc kontrolki
        btExecuteQuery.setEnabled(true);
        sw.stop();
        String timeInMs = String.valueOf(sw.getElapsedTime());
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        progressBar.setVisibility(View.GONE);
        //TODO: zrobic paginacje
        int rowsInTable = rows;
        String total ="";
        if(rows >=100){
            rowsInTable = rows > 100 ? 100 : rows;
            total = " of total " + rows;
        }

        CreateTable(output,rowsInTable,columns);
        if(output != null && output.size() > 0)
            Toast.makeText(getApplicationContext(), getText(R.string.downloaded)+": "+rowsInTable+total+" rows"+" (in "+timeInMs+" ms)", Toast.LENGTH_LONG).show();
        if(output.size() == 0 && rows > 0)
            Toast.makeText(getApplicationContext(), rows + " "+"row(s) affected."+" Finished"+" in "+timeInMs+" ms)", Toast.LENGTH_LONG).show();
    }

    @Override
    public void processFinishWithError(String error) {
        btExecuteQuery.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        if (!error.equals(ConnectionClass.CONNECTION_SUCCESS))
            Toast.makeText(context, getText(R.string.error_occurred)+": " + error, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, getText(R.string.success) + error, Toast.LENGTH_LONG).show();
    }
    private void CreateTable(ArrayList<String> list, int rows, int columns) {
        //TODO: dodac numeracje wierszy
        TableRow newRow;
        int elementNumber = 0;
        if (tableLayout != null) {
            tableLayout.removeAllViewsInLayout();
        }
        //TODO: wyczyscic komentarze
        try {
            for (int i = 0; i <= rows; i++) {  //dla kazdego wiersza
                newRow = new TableRow(this);
                newRow.setPadding(2, 2, 2, 2);
                if (i == 0)
                    newRow.setBackgroundColor(ContextCompat.getColor(context,R.color.tableColorHeader));
                else if (i % 2 == 0)
                    newRow.setBackgroundColor(ContextCompat.getColor(context,R.color.tableColorFirst)); //#6F9C33

                for (int j = 0; j < columns+1; j++) {  //dla kazdej kolumny
                    TextView tv = new TextView(this);
                    if(i== 0 )
                        tv.setTypeface(Typeface.DEFAULT_BOLD);
                    if(j == 0 ) {
                        tv.setTypeface(Typeface.DEFAULT_BOLD);
                        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT, 1f);
                        tv.setLayoutParams(params);
                        tv.setMaxWidth(1500);
                        tv.setPadding(4, 2, 4, 2);
                        tv.setText(i == 0 ? "" : String.valueOf(i));
                        newRow.addView(tv);
                        continue;
                    }
                    if (j % 2 != 0 && i != 0) {
                        tv.setBackgroundColor(ContextCompat.getColor(context,R.color.tableColorSecond));
                    }
                    TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT, 1f);
                    tv.setLayoutParams(params);
                    tv.setMaxWidth(1500);
                    tv.setPadding(4, 2, 4, 2);
                    tv.setText(list.get(elementNumber++));
                    newRow.addView(tv);
                }
                tableLayout.addView(newRow);
            }
        } catch (Exception e) {
            Log.d("ERROR: ", "Error during table creation- " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Error during table creation- " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        horizontalScrollView.fullScroll(HorizontalScrollView.FOCUS_UP);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
