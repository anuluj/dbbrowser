package com.example.mateusz.myapplication.enums;

/**
 * Created by mateg on 06.09.2017.
 */


public enum DbTypes {
    MSSQL(1), POSTGRES(2), MYSQL(3);
    private final int val;

    DbTypes(int val){
        this.val = val;
    }

    public int getVal(){
        return this.val;
    }
}

