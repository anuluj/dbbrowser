package com.example.mateusz.myapplication.models.database

data class DbConstrain(var primaryTableName: String, var primaryColumnName: String, var foreignTableName: String,
                  var foreignColumnName: String, var constrainName: String)
