package com.example.mateusz.myapplication.utils;

import android.util.Log;

import com.example.mateusz.myapplication.interfaces.ConnectionResponse;
import com.example.mateusz.myapplication.models.Credentials;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by mateg on 11.11.2016.
 */

public class ConnectionClass {

    public static String CONNECTION_SUCCESS = "Connection success";
    private String connectionStatus = CONNECTION_SUCCESS;
    private String className ="";
    private String connURL;
    private Properties props;
    public ConnectionResponse delegate = null;
    public ConnectionClass(Credentials credentials){
        this.props = new Properties();
        switch (credentials.getDbType()){
            case MSSQL:
                //this.className = "net.sourceforge.jtds.jdbc.Driver";
                this.connURL = "jdbc:jtds:sqlserver://"+credentials.getIp();
                if(credentials.getUseSSL()) props.setProperty("ssl","request");
                break;
            case MYSQL:
                //this.className = "com.mysql.jdbc.Driver";
                this.connURL = "jdbc:mysql://"+credentials.getIp()+"/"+credentials.getDbName();
                if(credentials.getUseSSL()) props.setProperty("useSsl","true");
                props.setProperty("host",credentials.getIp());
                break;
            case POSTGRES:
                //this.className = "org.postgresql.Driver";
                this.connURL ="jdbc:postgresql://"+credentials.getIp()+"/"+credentials.getDbName();
                if(credentials.getUseSSL()) props.setProperty("ssl","true");
                break;
            default:
                connectionStatus = "JDBC driver error";
                Log.e("ERROR", "JDBC driver error");
                return;
        }
        props.setProperty("databaseName", credentials.getDbName());
        props.setProperty("port",credentials.getPort());
        props.setProperty("user",credentials.getUser());
        props.setProperty("password", credentials.getPassword());
    }
    public Connection CONN() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(connURL,props);
        } catch (SQLException se) {
            Log.e("ERRO 1", se.getMessage());
            connectionStatus = se.getMessage();
        } catch (Exception e) {
            Log.e("ERRO 2", e.getMessage());
            connectionStatus = e.getMessage();
        }
        delegate.connectionStatus(connectionStatus);
        return conn;
    }
}
