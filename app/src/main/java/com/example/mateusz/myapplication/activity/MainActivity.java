package com.example.mateusz.myapplication.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mateusz.myapplication.utils.ConnectionHandler;
import com.example.mateusz.myapplication.interfaces.DbInfoResponse;
import com.example.mateusz.myapplication.utils.QueryExecutor;
import com.example.mateusz.myapplication.interfaces.QueryResponse;
import com.example.mateusz.myapplication.R;
import com.example.mateusz.myapplication.models.database.DbInfo;
import com.example.mateusz.myapplication.enums.QueryType;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DbInfoResponse, QueryResponse {

    Button btERD, btQueryActivity, btListOfTables;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btERD = findViewById(R.id.btERD);
        btQueryActivity = findViewById(R.id.btQueryActivity);
        btListOfTables = findViewById(R.id.btListOfTables);
        btERD.setOnClickListener(this);
        btQueryActivity.setOnClickListener(this);
        btListOfTables.setOnClickListener(this);

        if(((ConnectionHandler)getApplication()).getDbInfo() == null) {
            QueryExecutor queryExecutor;
            queryExecutor = new QueryExecutor(getApplication().getApplicationContext(), ((ConnectionHandler) getApplication()).getCredentials(), QueryType.GET_DB_INFO);
            queryExecutor.dbInfoResponse = MainActivity.this;
            queryExecutor.delegate = MainActivity.this;
            queryExecutor.execute();
        }
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch(v.getId()){
            case R.id.btERD:
                i = new Intent(this, WebViewActivity.class);
                startActivity(i);
                break;
            case R.id.btQueryActivity:
                i = new Intent(this, SqlQueryActivity.class);
                startActivity(i);
                break;
            case R.id.btListOfTables:
                i = new Intent(this, ListOfTablesActivity.class);
                startActivity(i);
                break;
        }

    }

    @Override
    public void returnDbInfo(DbInfo dbInfo) {
        ((ConnectionHandler)getApplication()).setDbInfo(dbInfo);
    }

    @Override
    public void processFinish(ArrayList<String> output, int rows, int columns) {

    }

    @Override
    public void processFinishWithError(String error) {

    }
}
