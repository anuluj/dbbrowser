package com.example.mateusz.myapplication.interfaces;

import java.util.ArrayList;

public interface QueryResponse {
    void processFinish(ArrayList<String> output, int rows, int columns);
    void processFinishWithError(String error);
}
