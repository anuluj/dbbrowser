package com.example.mateusz.myapplication.enums;

/**
 * Created by mlinke on 2017-12-20.
 */

public enum QueryType {
    GET_DB_INFO,
    LOGIN,
    NORMAL_QUERY
}
