package com.example.mateusz.myapplication.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.example.mateusz.myapplication.R;
import com.example.mateusz.myapplication.interfaces.DbInfoResponse;
import com.example.mateusz.myapplication.interfaces.QueryResponse;
import com.example.mateusz.myapplication.interfaces.ConnectionResponse;
import com.example.mateusz.myapplication.models.database.DbColumn;
import com.example.mateusz.myapplication.models.database.DbConstrain;
import com.example.mateusz.myapplication.models.database.DbInfo;
import com.example.mateusz.myapplication.models.database.DbTable;
import com.example.mateusz.myapplication.enums.QueryType;
import com.example.mateusz.myapplication.models.Credentials;
import com.example.mateusz.myapplication.utils.ConnectionClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class QueryExecutor extends AsyncTask<String, Void, DbInfo> implements ConnectionResponse {

    public QueryResponse delegate = null;
    public DbInfoResponse dbInfoResponse = null;
    private String query, message;
    private QueryType queryType;
    private int columns = 0, rows = 0;
    private Connection conn;
    private boolean isSuccess = false, isConnectionError = false;
    private Credentials mCredentials;
    private ArrayList<String> list = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    private Context mContext;

    public QueryExecutor(String query, Credentials credentials, QueryType typeOfQuery) {
        this.mCredentials = credentials;
        this.query = query;
        this.queryType = typeOfQuery;
    }

    public QueryExecutor(Context context, Credentials credentials, QueryType typeOfQuery) {
        this.mCredentials = credentials;
        this.queryType = typeOfQuery;
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected DbInfo doInBackground(String... params) {
        //   PROCESSING
        ResultSet rs;

        ConnectionClass connectionClass = new ConnectionClass(mCredentials);
        connectionClass.delegate = this;

        try {
            conn = connectionClass.CONN();

            if (queryType == QueryType.GET_DB_INFO && conn != null) {
                String queryGetDbName = "", queryGetTablesAndColumns = "",queryGetRelations = "", dbName = "", tableName = "";
                Resources resources = mContext.getResources();

                switch (mCredentials.getDbType()) {
                    case MSSQL:
                        queryGetDbName = resources.getString(R.string.query_mssql_get_db_name);
                        queryGetTablesAndColumns = resources.getString(R.string.query_mssql_get_tables_and_columns);
                        queryGetRelations = resources.getString(R.string.query_mssql_get_relations);
                        break;
                    case POSTGRES:
                        queryGetDbName = resources.getString(R.string.query_postgresql_get_db_name);
                        queryGetTablesAndColumns = resources.getString(R.string.query_postgresql_get_tables_and_columns);
                        queryGetRelations = resources.getString(R.string.query_postgresql_get_relations);
                        break;
                    case MYSQL:
                        queryGetDbName = resources.getString(R.string.query_mysql_get_db_name);
                        queryGetTablesAndColumns = resources.getString(R.string.query_mysql_get_tables_and_columns);
                        queryGetRelations = resources.getString(R.string.query_mysql_get_relations);
                        break;
                }

                ArrayList<DbTable> dbTables = new ArrayList<>();
                ArrayList<DbColumn> dbColumns = new ArrayList<>();
                ArrayList<DbConstrain> dbConstrains = new ArrayList<>();

                Statement stmt = conn.createStatement();

                ResultSet resultSet = stmt.executeQuery(queryGetDbName);
                if (resultSet.next()) { // READ DB NAME
                    dbName = resultSet.getString(1);
                }


                resultSet = stmt.executeQuery(queryGetTablesAndColumns);
                if (resultSet.next()) { //READ TABLES AND COLUMNS
                    tableName = resultSet.getString(1);
                    dbColumns.add(new DbColumn(resultSet.getString(2), resultSet.getString(3)));
                }
                while (resultSet.next()) {
                    if (tableName.equalsIgnoreCase(resultSet.getString(1))) {
                        dbColumns.add(new DbColumn(resultSet.getString(2), resultSet.getString(3)));
                    } else {
                        dbTables.add(new DbTable(tableName, dbColumns));
                        dbColumns = new ArrayList<>();
                        tableName = resultSet.getString(1);
                        dbColumns.add(new DbColumn(resultSet.getString(2), resultSet.getString(3)));
                    }
                }
                dbTables.add(new DbTable(tableName, dbColumns));

                //READ RELATIONS
                resultSet = stmt.executeQuery(queryGetRelations);
                while (resultSet.next()) {
                    dbConstrains.add(new DbConstrain(resultSet.getString(1),
                                                    resultSet.getString(2),
                                                    resultSet.getString(3),
                                                    resultSet.getString(4),
                                                    resultSet.getString(5)));
                }
                isSuccess = true;
                return new DbInfo(dbName, dbTables, dbConstrains);
            }

            if (queryType == QueryType.NORMAL_QUERY && conn != null) {
                Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                if (stmt.execute(query)) {
                    rs = stmt.getResultSet();
                    ResultSetMetaData rsmd = rs.getMetaData();
                    columns = rsmd.getColumnCount();

                    for (int i = 1; i <= columns; i++) {
                        list.add(!rsmd.getColumnName(i).isEmpty() ? rsmd.getColumnName(i) : "NoName");
                    }
                    while (rs.next()) {
                        rows++;
                        for (int i = 1; i <= columns; i++) {
                            if (rsmd.getColumnType(i) == Types.BLOB)
                                list.add("BLOB");
                            else
                                list.add(rs.getString(i) != null ? rs.getString(i) : "NULL");
                        }
                        if (rows >= 100)
                            break;
                    }
                    rs.last();
                    rows = rs.getRow();
                } else {
                    rows = stmt.getUpdateCount();
                }
                isSuccess = true;
            }
        } catch (SQLException sqlE) {
            Log.d("ERROR: ", sqlE.getMessage());
            isSuccess = false;
            message = sqlE.getMessage();
        } catch (Exception e) {
            Log.d("ERROR: ", e.getMessage());
            isSuccess = false;
            message = e.getMessage();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(DbInfo dbInfo) {
        super.onPostExecute(dbInfo);
        if (isConnectionError || !isSuccess) {
            delegate.processFinishWithError(this.message);
            return;
        }
        if (queryType == QueryType.GET_DB_INFO && isSuccess) {
            dbInfoResponse.returnDbInfo(dbInfo);
            return;
        }
        delegate.processFinish(list, rows, columns);
    }

    @Override
    public void connectionStatus(String message) {
        if (!message.equals(ConnectionClass.CONNECTION_SUCCESS)) {
            this.message = message;
            isConnectionError = true;
        } else {
            this.message = ConnectionClass.CONNECTION_SUCCESS;
        }
        Log.d(TAG, "connectionStatus: delegataConnection");
    }
}
