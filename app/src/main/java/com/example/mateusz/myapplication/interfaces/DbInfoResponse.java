package com.example.mateusz.myapplication.interfaces;

import com.example.mateusz.myapplication.models.database.DbInfo;

public interface DbInfoResponse {
    void returnDbInfo(DbInfo dbInfo);
}
