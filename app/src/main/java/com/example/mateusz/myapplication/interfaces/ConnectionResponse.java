package com.example.mateusz.myapplication.interfaces;

public interface ConnectionResponse {
    void connectionStatus(String error);
}
