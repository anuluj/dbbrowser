package com.example.mateusz.myapplication.utils

import android.app.Application
import com.example.mateusz.myapplication.models.database.DbInfo
import com.example.mateusz.myapplication.models.Credentials

class ConnectionHandler(var credentials: Credentials? = null, var dbInfo: DbInfo? = null) : Application()
