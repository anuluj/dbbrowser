package com.example.mateusz.myapplication.models.database

import java.util.ArrayList

data class DbInfo (val name: String, val tables: ArrayList<DbTable>, val constrains: ArrayList<DbConstrain>){
    fun getTablesNames() : List<String>{
        return tables.map { x ->  x.name}
    }
    fun getFlatColumnNames() : List<String>{
        return tables.flatMap{it.columns.map { it.name }}.distinct()
    }
    fun getColumnNameAndTypePair() :List<List<Pair<String,String>>>{
        return tables.map { it.columns.map{ Pair(it.name, it.dataType)} }
    }
}
