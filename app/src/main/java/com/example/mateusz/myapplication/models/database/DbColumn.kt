package com.example.mateusz.myapplication.models.database
data class DbColumn(val name: String, val dataType: String)