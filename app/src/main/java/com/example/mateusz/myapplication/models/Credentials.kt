package com.example.mateusz.myapplication.models

import com.example.mateusz.myapplication.enums.DbTypes

data class Credentials(val ip: String, val dbName: String, val user: String, val password: String, val port: String, val dbType: DbTypes, val useSSL: Boolean = false)
