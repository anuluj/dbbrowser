package com.example.mateusz.myapplication.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.mateusz.myapplication.utils.ConnectionHandler;
import com.example.mateusz.myapplication.interfaces.DbInfoResponse;
import com.example.mateusz.myapplication.utils.QueryExecutor;
import com.example.mateusz.myapplication.interfaces.QueryResponse;
import com.example.mateusz.myapplication.R;
import com.example.mateusz.myapplication.models.database.DbColumn;
import com.example.mateusz.myapplication.models.database.DbConstrain;
import com.example.mateusz.myapplication.models.database.DbInfo;
import com.example.mateusz.myapplication.models.database.DbTable;
import com.example.mateusz.myapplication.enums.QueryType;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class WebViewActivity extends Activity implements DbInfoResponse, QueryResponse{

    String dot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        DbInfo dbInfo =  ((ConnectionHandler)getApplication()).getDbInfo();
        if(dbInfo != null) {
            loadWebView(dbInfo);
        }
        else{
            QueryExecutor queryExecutor;
            queryExecutor = new QueryExecutor(getApplication().getApplicationContext(), ((ConnectionHandler) getApplication()).getCredentials(), QueryType.GET_DB_INFO);
            queryExecutor.dbInfoResponse = WebViewActivity.this;
            queryExecutor.delegate = WebViewActivity.this;
            queryExecutor.execute();
        }
    }
    private String readAssetFile(String fileName) {
        try {
            StringBuilder buffer = new StringBuilder();
            InputStream fileInputStream = getAssets().open(fileName);
            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
            String str;

            while ((str = bufferReader.readLine()) != null) {
                buffer.append(str);
            }
            fileInputStream.close();

            return buffer.toString();
        }catch (Exception e){
            Log.d("readAssetFileError: ", e.getMessage());
        }
        return "Error";
    }
    public String getDotString(DbInfo db){
        String dot =
                "graph g { graph [fontsize=30 label=\\\\\""+db.getName()+"\\\\\" labelloc= \\\\\"t\\\\\"splines=ortho overlap=false rankdir = LR ratio = compress;]  ;";

        for(DbTable table : db.getTables()){
            dot += "\\\\\""+ table.getName() +"\\\\\"[ shape=\\\\\"none\\\\\" label=<<table border=\\\\\"0\\\\\" bgcolor=\\\\\"white\\\\\" cellspacing=\\\\\"0\\\\\">"+
                    "<tr><td bgcolor=\\\\\"black\\\\\" align=\\\\\"left\\\\\" border=\\\\\"1\\\\\" colspan=\\\\\"0\\\\\"><font color=\\\\\"white\\\\\">" + table.getName() +"</font></td></tr>";
            for(DbColumn column : table.getColumns()){
                dot+=
                        "<tr><td width = \\\\\"120%\\\\\" align=\\\\\"left\\\\\" tvPort=\\\\\" "
                                +
                                table.getName()+"_"+column.getName()
                                +
                                "\\\\\" border=\\\\\"1\\\\\">"
                                +
                                column.getName()
                                +
                                " ("+ column.getDataType() + ")"
                                +
                                "</td></tr>";
            }
            dot += "</table>> ]";
        }
        for(DbConstrain constrain : db.getConstrains()){
            dot +="\\\\\""+constrain.getPrimaryTableName()+"\\\\\""+" -- "+"\\\\\""+ constrain.getForeignTableName()+"\\\\\""+" [];";
        }
        dot+="}";
        return dot;
    }

    @Override
    public void returnDbInfo(DbInfo dbInfo) {
        ((ConnectionHandler)getApplication()).setDbInfo(dbInfo);
        loadWebView(dbInfo);
    }

    private void loadWebView(DbInfo dbInfo) {
        dot = getDotString(dbInfo);
        WebView webView = findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSansSerifFontFamily("sans-serif");
        webSettings.setDomStorageEnabled(true);
        webSettings.setBuiltInZoomControls(true);

        String newHtml;
        newHtml = readAssetFile("index.html");
        if(!newHtml.equals("Error")){
            newHtml = newHtml.replaceFirst("string_to_replace", dot);
        }
        webView.loadDataWithBaseURL("file:///android_assets/", newHtml, "text/html", "UTF-8", null);
    }

    @Override
    public void processFinish(ArrayList<String> output, int rows, int columns) {
    }

    @Override
    public void processFinishWithError(String error) {
        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
    }
}
