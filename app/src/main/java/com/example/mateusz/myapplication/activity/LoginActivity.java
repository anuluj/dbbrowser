package com.example.mateusz.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.mateusz.myapplication.utils.ConnectionClass;
import com.example.mateusz.myapplication.utils.ConnectionHandler;
import com.example.mateusz.myapplication.models.Credentials;
import com.example.mateusz.myapplication.R;
import com.example.mateusz.myapplication.interfaces.ConnectionResponse;
import com.example.mateusz.myapplication.enums.DbTypes;

import java.sql.Connection;

public class LoginActivity extends AppCompatActivity{
    Spinner spinner_select_db;
    Button btLogin;
    Context context;
    EditText tvDbName, tvHostName, tvUserName, tvPassword, tvPort;
    TextInputLayout textInputEditTextPassword;
    ProgressBar progressBar;
    Switch switchSSL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = getApplicationContext();
        btLogin = findViewById(R.id.btLogin);
        progressBar = findViewById(R.id.progressBar_LoginActivity);
        tvDbName = findViewById(R.id.etLoginDatabaseName);
        tvHostName = findViewById(R.id.etLoginHost);
        tvUserName = findViewById(R.id.etLoginUser);
        tvPassword = findViewById(R.id.etLoginPassword);
        tvPort = findViewById(R.id.etLoginPort);
        switchSSL = findViewById(R.id.switch_SSL);

        textInputEditTextPassword = findViewById(R.id.PasswordTextInput);

        //TEST INIT

        tvDbName.setText("northwind");
        tvHostName.setText("192.168.0.12");
        tvUserName.setText("andro_login");
        tvPassword.setText("andro");

        //SPINNER INIT
        spinner_select_db = findViewById(R.id.spinner_selectDb);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.db_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_select_db.setAdapter(adapter);
        spinner_select_db.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (DbTypes.values ()[(int) id]) {
                    case MSSQL:
                        tvPort.setText(getText(R.string.default_mssql_port));
                        break;
                    case POSTGRES:
                        tvPort.setText(getText(R.string.default_postgres_port));
                        break;
                    case MYSQL:
                        tvPort.setText(getText(R.string.default_myssql_port));
                        break;
                    default:
                        break;
                }
                Log.d("dbType", "onItemSelected: "+parent.getSelectedItemId());
                Log.d("dbTypEnum", DbTypes.values()[(int) id].toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        tvPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0)
                    textInputEditTextPassword.setPasswordVisibilityToggleEnabled(false);
                else
                    textInputEditTextPassword.setPasswordVisibilityToggleEnabled(true);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fields validation
                if(tvDbName.getText().length() == 0){
                    tvDbName.setError(getText(R.string.error_empty_edit_text));
                }
                 if(tvHostName.getText().length() == 0){
                    tvHostName.setError(getText(R.string.error_empty_edit_text));
                }
                 if(tvUserName.getText().length() == 0){
                    tvUserName.setError(getText(R.string.error_empty_edit_text));
                }
                 if(tvPassword.getText().length() == 0){
                    tvPassword.setError(getText(R.string.error_empty_edit_text));
                }
                 if(tvPort.getText().length() == 0){
                    tvPort.setError(getText(R.string.error_empty_edit_text));
                }else if(!TextUtils.isDigitsOnly(tvPort.getText())){
                     tvPort.setError(getText(R.string.error_is_not_numeric));
                 }

                if(tvDbName.getError()==null &&
                        tvHostName.getError() == null &&
                        tvUserName.getError() == null &&
                        tvPassword.getError() == null &&
                        tvPort.getError() == null){
                    Credentials credentials = new Credentials(tvHostName.getText().toString(),
                            tvDbName.getText().toString(),
                            tvUserName.getText().toString(),
                            tvPassword.getText().toString(),
                            tvPort.getText().toString(),
                            DbTypes.values()[(int)spinner_select_db.getSelectedItemId()],
                            switchSSL.isChecked());
                    enableControls(false);
                    Login loginExecutor = new Login(credentials);
                    loginExecutor.execute();
                }
                else {
                    Toast.makeText(getApplicationContext(), "ERROR!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private void enableControls(boolean enable){
        if(enable)
            progressBar.setVisibility(View.GONE);
        else
            progressBar.setVisibility(View.VISIBLE);
        spinner_select_db.setEnabled(enable);
        tvDbName.setEnabled(enable);
        tvHostName.setEnabled(enable);
        tvUserName.setEnabled(enable);
        tvPassword.setEnabled(enable);
        tvPort.setEnabled(enable);
        btLogin.setEnabled(enable);
        switchSSL.setEnabled(enable);
    }


    public class Login extends AsyncTask<Void,Void,Void> implements ConnectionResponse{
        private Credentials _credentials = null;
        private ConnectionClass connectionClass;
        private String message = "";

        public Login(Credentials credentials){
            this._credentials = credentials;
        }
        @Override
        protected Void doInBackground(Void... params) {
            connectionClass = new ConnectionClass(_credentials);
            connectionClass.delegate = this;

            try {
                connectionClass.CONN();
            } catch (Exception e) {
                Log.d("conn", " error: " + e);
                message = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            enableControls(true);
            Toast.makeText(context,message,Toast.LENGTH_LONG).show();
            if(message.equals(ConnectionClass.CONNECTION_SUCCESS)) {
                ((ConnectionHandler)getApplication()).setCredentials(_credentials);
                Intent i =  new Intent(context,MainActivity.class);
                startActivity(i);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public void connectionStatus(String connectionStatus) {
            Log.d("LoginAsync ","connection error");
            if(connectionStatus.equals(ConnectionClass.CONNECTION_SUCCESS)){
                message = getString(R.string.connection_success);
            }
            else{
                message = getString(R.string.error_occurred)+": "+connectionStatus;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
