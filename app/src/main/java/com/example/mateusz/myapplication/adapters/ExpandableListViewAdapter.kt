package com.example.mateusz.myapplication.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.example.mateusz.myapplication.R

class ExpandableListViewAdapter constructor(private val groupNames: List<String>, private val childNames: List<List<Pair<String,String>>>, private val context: Context): BaseExpandableListAdapter() {

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var view = convertView

        if(view == null){
            val li = LayoutInflater.from(context)
            view = li.inflate(R.layout.list_of_tables_table_name, null)
        }
        val item  = groupNames[groupPosition]

        view?.findViewById<TextView>(R.id.tv_list_of_tables_table_name)?.text = item

        return view
    }
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var view = convertView

        if(view == null){
            val li = LayoutInflater.from(context)
            view = li.inflate(R.layout.list_of_tables_column_name, null)
        }
        val item  = childNames[groupPosition][childPosition]

        view?.findViewById<TextView>(R.id.tv_list_of_tables_column_name)?.text = item.first
        view?.findViewById<TextView>(R.id.tv_list_of_tables_column_type)?.text = item.second

        return view
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return childNames[groupPosition].size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return childNames.get(groupPosition).get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
         return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return groupNames.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return groupNames[groupPosition]
    }
}